import * as API from './../utils/api/comment'

export const GET_COMMENTS = 'GET_COMMENTS'
export const ADD_COMMENT = 'ADD_COMMENT'
export const LOGGED_OUT = 'LOGGED_OUT'
export const DELETE_COMMENT = 'DELETE_COMMENT'

export function getComments({data}){
  return{
    type: GET_COMMENTS,
    data
  }
}

export function getCommentsAPI(postid){
  return function(dispatch){
    return API.getComments(postid)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutComment())
      }
      else if(!api_data.error){
        dispatch(getComments({data: api_data.data}))
      }
      return api_data
    })
  }
}

function addComment({comment}){
  return {
    type: ADD_COMMENT,
    comment,
  }
}

export function addCommentAPI({body, poster, userId, postid, parentId, topicId}){
  return function(dispatch){
    return API.addComment(body, poster, userId, postid, parentId, topicId)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutComment())
      }
      else if(!api_data.error){
        dispatch(addComment({comment: api_data.data}))
      }
      return api_data
    })
  }
}

export function deleteComment({id}){
  return {
    type: DELETE_COMMENT,
    id,
  }
}

export function deleteCommentAPI({id}){
  return function(dispatch){
    return API.deleteComment(id)
    .then(api_data => {
      if(!api_data.user){
        dispatch(loggedOutComment())
      }
      else if(!api_data.error){
        dispatch(deleteComment({id}))
      }
      return api_data
    })
  }
}

export function loggedOutComment(){
  return{
    type: LOGGED_OUT
  }
}
