import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {checkForLoggedIn} from './../actions/user'

class AuthOnly extends Component{

  componentDidMount(){
    if(!this.props.user.userLoggedIn){
      this.props.checkForLoggedIn()
      .then(data => {
        if(!data.user){
          this.props.history.push('/login')
        }else if(!data.user.confirmed){
          this.props.history.replace('/profile')
        }
      })
    }
    else if(!this.props.user.confirmed){
      this.props.history.replace('/profile')
    }
  }

  componentWillReceiveProps(nextProps){
    if(this.props.user.userLoggedIn && !nextProps.user.userLoggedIn){
      this.props.history.push('/login')
    }

  }

  render(){
    return(<span></span>)
  }
}

function mapStateToProps({user}, ownProps){

  return {
    user: user
  }
}

function mapDispachToProps(dispatch){
  return{
    checkForLoggedIn: (data) => dispatch(checkForLoggedIn(data))
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(AuthOnly));
