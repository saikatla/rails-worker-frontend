import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {Image} from 'semantic-ui-react'

import {getAvatarImage} from './../utils/helpers'

class Avatar extends Component{
  state = {

  }

  render(){
    let name = this.props.user.first + ' ' + this.props.user.last
    if(this.props.name)
      name = this.props.name

    let size = this.props.size ? this.props.size : 80
    let background = this.props.background ? this.props.background : [140,140,140,0]
    var data = getAvatarImage(name, size, background)
    return(
      <Image src={data} alt='Randomly generated avatar'/>
    )
  }
}

function mapStateToProps({user}, ownProps){
  return {
    user: user
  }
}

function mapDispachToProps(dispatch){
  return{

  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(Avatar));
