import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'

import {Modal, Form, Grid, Button} from 'semantic-ui-react'

import {updatePasswordAPI} from './../actions/user'

class ChangePasswordModal extends Component{
  state = {
    error: '',
    showModal: false,
    oldPassword: '',
    newPassword: '',
    confirm: ''
  }

  changeModal = () => {
    this.setState({showModal: !this.state.showModal})
  }

  onChangePassword = () => {
    let {oldPassword, newPassword} = this.state
    this.props.updatePassword({oldPassword, newPassword})
    .then(d => {
      console.log(d)
      if(d.error)
        this.setState({
          error: d.error,
          oldPassword: '',
          newPassword: '',
          confirm: ''
        })
      else{
        this.setState({
          showModal: false,
          error: '',
          oldPassword: '',
          newPassword: '',
          confirm: ''
        })
      }
    })
  }
  validate(){
    let {newPassword, oldPassword, confirm} = this.state
    if(!newPassword || !oldPassword || !confirm)
      return false
    else if(newPassword.length < 6 || !/\d/.test(newPassword)){
      return false
    }
    else if(newPassword !== confirm){
      return false
    }
    return true
  }

  render(){



    return(
      <Modal
        trigger={<Button compact primary size='mini' onClick={this.changeModal}>Update</Button>}
        closeOnRootNodeClick={false}
        closeIcon
        onClose={this.changeModal}
        open={this.state.showModal}
        size='mini'
      >
        <Modal.Header>Change Password</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {this.state.error && (
              <h4>{this.state.error}</h4>
            )}
            <Form onSubmit={this.onChangePassword}>
              <Form.Field>
                <label>Old Password</label>
                <input
                  type='password'
                  onChange={(i) => this.setState({oldPassword: i.target.value})}
                  value={this.state.oldPassword}
                />
              </Form.Field>
              <Form.Field>
                <label>New Password</label>
                <input
                  type='password'
                  onChange={(i) => this.setState({newPassword: i.target.value})}
                  value={this.state.newPassword}
                />
              </Form.Field>
              <Form.Field>
                <label>Confirm New Password</label>
                <input
                  type='password'
                  onChange={(i) => this.setState({confirm: i.target.value})}
                  value={this.state.confirm}
                />
              </Form.Field>

              <Grid>
                <Grid.Column textAlign='left' width='8'>
                  <Button
                    primary
                    type='submit'
                    disabled={!this.validate()}
                  >
                    Update
                  </Button>
                </Grid.Column>
                <Grid.Column textAlign='right' width='8'>
                  <Button
                    negative
                    onClick={this.changeModal}
                  >
                    Cancel
                  </Button>
                </Grid.Column>

              </Grid>

            </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    )
  }
}

function mapStateToProps({post, topic, user}, ownProps){

  return {
    posts: post,
    user: user
  }
}

function mapDispachToProps(dispatch){
  return{
    updatePassword: (data) => dispatch(updatePasswordAPI(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(ChangePasswordModal));
