import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {Container, Form, Button, Comment, Modal, Header, Icon} from 'semantic-ui-react'

import {getCommentsAPI, addCommentAPI, deleteCommentAPI} from './../actions/comment'
import {timeSince, getAvatarImage} from './../utils/helpers'

class CommentThread extends Component{
  state = {
    commentBody: '',
    replyBoxId: 0,
    replyModalOpen: false,
    childCommentBody: '',
    collapsed: [],
    showDeleteModal: false,
    deleteCommentId: 0
  }

  componentDidMount(){

    this.props.getComments(this.props.postid)
    .then(data => {
      if(!data.user){
        this.props.history.replace('/login')
      }
    })
  }

  addComment = () => {
    let poster = `${this.props.user.first} ${this.props.user.last}`
    let userId = this.props.user.id

    let postid = this.props.postid
    let topicId = this.props.topic.id
    let body = this.state.commentBody
    if(body){
      this.props.addComment({body, poster, topicId, userId, postid, parentId: null})

      this.setState({commentBody: ''})
    }
  }

  addChild = () => {
    let parentId = this.state.replyBoxId
    let poster = `${this.props.user.first} ${this.props.user.last}`
    let userId = this.props.user.id
    let postid = this.props.postid
    let topicId = this.props.topic.id
    let body = this.state.childCommentBody
    if(body){
      this.props.addComment({body, poster, userId, postid, parentId, topicId})
      .then(() => this.setState({replyBoxId: 0, childCommentBody: ''}))

    }
  }

  addReplyBox = (id) => {
    if(this.state.replyBoxId === id){
      this.setState({replyBoxId: 0, childCommentBody: ''})
    }else{
      this.setState({replyBoxId: id})
    }
  }

  collapseComments = (id) => {

    if(this.state.collapsed.includes(id)){
      let a = this.state.collapsed
      let i = a.indexOf(id)
      a.splice(i, 1)
      this.setState({collapsed: a})
    }else{
      let a = this.state.collapsed
      a.push(id)
      this.setState({collapsed: a})
    }

  }

  deleteComment = () => {
    console.log('delete comment')
    this.props.deleteComment({id: this.state.deleteCommentId})
    this.setState({showDeleteModal: false, deleteCommentId: 0})
  }

  getThread = (c) => {
    let children = this.props.comments.filter(child => child.parentId === c.id)

    let s = this.state.collapsed.includes(c.id) ? 'Show' : 'Hide'

    const commentBox = (
      <Form reply>
        <Form.TextArea
          autoHeight={true}
          onChange={(i) => this.setState({childCommentBody: i.target.value})}
          value={this.state.childCommentBody}
        />
        <Button
          size='mini'
          content='Add Comment'
          onClick={() => this.addChild()}
          primary
        />
        <Button
          size='mini'
          content='Cancel'
          onClick={() => this.setState({replyBoxId: 0, childCommentBody: ''})}
          negative
        />
      </Form>
    )
    return(
      <Comment key={c.id}>
        <Comment.Avatar src={getAvatarImage(c.poster)}/>

        <Comment.Content>
          <Comment.Author as='span'>{c.poster}</Comment.Author>
          <Comment.Metadata>
            <span>{timeSince(c.createdAt)}</span>
          </Comment.Metadata>
          <Comment.Text style={{whiteSpace: 'pre-line'}}>{c.body}</Comment.Text>
          <Comment.Actions>
              {!c.deleted && (
                <a onClick={() => this.addReplyBox(c.id)}>Reply</a>
              )}
              {children.length > 0 && (
                <a onClick={() => this.collapseComments(c.id)}>{s}</a>
              )}
              {!c.deleted && this.props.user.id === c.userId && (
                <a onClick={() => this.setState({showDeleteModal: true, deleteCommentId: c.id})}>Delete</a>
              )}
          </Comment.Actions>
          {this.state.replyBoxId === c.id && (
            <span>{commentBox}</span>
          )}
        </Comment.Content>
        {children.length > 0 && (
          <Comment.Group collapsed={this.state.collapsed.includes(c.id)}>
            {children.map(i => this.getThread(i))}
          </Comment.Group>
        )}
      </Comment>
    )
  }

  render(){
    let parentComments = []
    if(this.props.comments){
      parentComments = this.props.comments.filter(comment => !comment.parentId).map(c => {
        return (
          this.getThread(c)
        )
      })
    }

    const deleteModal = (
      <Modal open={this.state.showDeleteModal} basic size='small'>
        <Header icon='trash outline' content='Delete Comment' />
        <Modal.Content>
          <p>Are you sure you want to delete this comment?  This action will not remove the comment, it will only delete the text.</p>
        </Modal.Content>
        <Modal.Actions>
          <Button color='red' inverted onClick={() => this.setState({showDeleteModal: false, deleteCommentId: 0})}>
            <Icon name='remove' /> No
          </Button>
          <Button color='green' inverted onClick={() => this.deleteComment()}>
            <Icon name='checkmark' /> Yes
          </Button>
        </Modal.Actions>
      </Modal>
    )

    return(
      <Container fluid>
        <h3>Comments</h3>
        {deleteModal}
          <Comment.Group threaded>
            {parentComments}
            <Form reply>
              <Form.TextArea
                autoHeight={true}
                onChange={(i) => this.setState({commentBody: i.target.value})}
                value={this.state.commentBody}
              />
              <Button
                content='Add Comment'
                onClick={() => this.addComment()}
                primary
              />
            </Form>
          </Comment.Group>
      </Container>
    )
  }
}

function mapStateToProps({post, topic, comment, user}, ownProps){
  let postid = ownProps.match.params.postid
  let location = ownProps.match.params.topicLocation
  let t = topic.find(item => item.location === location)

  return {
    postid: postid,
    topicLocation: location,
    comments: comment,
    user: user,
    topic: t
  }
}

function mapDispachToProps(dispatch){
  return{
    getComments: (data) => dispatch(getCommentsAPI(data)),
    addComment: (data) => dispatch(addCommentAPI(data)),
    deleteComment: (data) => dispatch(deleteCommentAPI(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(CommentThread));
