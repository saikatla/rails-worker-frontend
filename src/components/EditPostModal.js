import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import { Button, Modal, Form, TextArea, Grid, Icon } from 'semantic-ui-react'

import {sendPostAPI} from './../actions/post'

class EditPostModal extends Component{
  constructor(props) {
    super(props);

    this.state = {
      showEditModal: false,
      body: props.post.body,
      error: ''
    }
  }

  changeModal = () => {
    this.setState({showEditModal: !this.state.showEditModal})
  }

  editBody = () => {
    this.setState({showEditModal: false})
    this.props.onEdit(this.props.post.id, this.state.body)

  }

  render(){

    const editButton = (
      <Button
        compact
        basic
        fluid
        onClick={() => this.setState({showEditModal: true})}
      >
        <Icon
          name='edit'
        />
        Edit
      </Button>
    )

    return (

      <Modal
        trigger={editButton}
        closeOnRootNodeClick={false}
        closeIcon
        onClose={this.changeModal}
        open={this.state.showEditModal}
      >
        <Modal.Header>Edit Post</Modal.Header>
        <Modal.Content>
          <Modal.Description>
            {this.state.error && (
              <h4>{this.state.error}</h4>
            )}
          <Form onSubmit={this.editBody}>
            <Form.Field>
              <label>{this.props.post.title}</label>
            </Form.Field>
            <Form.Field>
              <label>Post</label>
              <TextArea
                autoHeight
                rows='5'
                onChange={(i) => this.setState({body: i.target.value})}
                value={this.state.body}
              />
            </Form.Field>

            <Grid>
              <Grid.Column textAlign='left' width='8'>
                <Button
                  primary
                  type='submit'
                  disabled={!(this.state.body)}
                >
                  Update
                </Button>
              </Grid.Column>
              <Grid.Column textAlign='right' width='8'>
                <Button
                  negative
                  onClick={this.changeModal}
                >
                  Cancel
                </Button>
              </Grid.Column>

            </Grid>

          </Form>
          </Modal.Description>
        </Modal.Content>
      </Modal>
    )
  }
}

function mapStateToProps({user, topic}, ownProps){
  return {
    topics: topic,
    user: user
  }
}

function mapDispachToProps(dispatch){
  return{
    sendPost: (data) => dispatch(sendPostAPI(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(EditPostModal));
