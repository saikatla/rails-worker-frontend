import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {Form, Message, Input, Container} from 'semantic-ui-react'

import {loggedInAPI} from './../actions/user'

class Login extends Component{
  state = {
    username: '',
    password: '',
    remember: false,
    error: 'Please Login to continue.'
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.user.userLoggedIn){
      this.props.history.replace('/dash')
    }
  }

  componentDidMount(){
    if(this.props.user.userLoggedIn){
      this.props.history.replace('/dash')
    }
  }

  sendLogin = () => {
    const {username, password, remember} = this.state
    this.props.loggedIn({username, password, remember})
      .then(data => {
        if(data.error){
          this.setState({error: data.error, password: ''})
        }
        else{
          this.props.history.push('/dash')
        }
      })
  }

  render() {
    const {username, password} = this.state

    const formInstance = (
      <Form onSubmit={this.sendLogin}>
        <Message
          negative={this.state.error !== 'Please Login to continue.'}
          content={this.state.error}
        />
        <Form.Field inline>
          <label>Username</label>
          <Input
            value={username}
            onChange={(i) => this.setState({username: i.target.value})}
            placeholder='Username'
          />
        </Form.Field>
        <Form.Field inline>
          <label>Password</label>
          <Input
            value={password}
            onChange={(i) => this.setState({password: i.target.value})}
            placeholder='Password'
            type='password'
          />
        </Form.Field>
        <Form.Checkbox
          label='Remember Me'
          onChange={(event, data) => this.setState({remember: data.checked})}
        />
        <Form.Button primary>Login</Form.Button>
      </Form>
    );


    return (
      <div>
        <Container textAlign='left'>
        {formInstance}
        </Container>
      </div>

    )
  }
}

function mapStateToProps({user}, ownProps){
  return {
    user: user
  }
}

function mapDispachToProps(dispatch){
  return{
    loggedIn: (data) => dispatch(loggedInAPI(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(Login));
