import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter} from 'react-router-dom'
import {Pagination, Container, Card} from 'semantic-ui-react'

import {getPostsFromAPI} from './../actions/post'

import PostCard from './PostCard'
import CommentCard from './CommentCard'

class PostCardGrid extends Component{
  state = {
    start: 0,
    end: 9,
    aPage: 1,
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.topicLocation !== this.props.topicLocation){
      this.setState({start: 0, end: 9, aPage: 1})
    }
  }

  onLastPage = () => {
    let {posts, total, topicLocation} = this.props
    let timestamp = posts[total-1].createdAt
    this.props.getPostsFrom({topicLocation, timestamp}).then(d => console.log(d))
  }

  handlePaginationChange = (e, { activePage }) => {
    let {pages} = this.props
    let newStart = (activePage - 1) * 9
    let newEnd = newStart + 9

    this.setState({start: newStart, end: newEnd, aPage: activePage})

    if(activePage >= pages)
      this.onLastPage()
  }

  render(){
    let {start, end} = this.state
    let {pages} = this.props

    let postlist = []

    if(this.props.fill === 'posts' && this.props.posts.length > 0 && this.props.topics){
      postlist = this.props.posts.slice(start, end)
      .map(p => <PostCard post={p} key={p.id}/>)
    }
    else if(this.props.fill === 'comments' && this.props.comments.length > 0 && this.props.topics){
      postlist = this.props.comments.slice(start, end)
      .map(c => <CommentCard comment={c} key={c.id}/>)
    }

    return(
      <Container>
        <Card.Group itemsPerRow={3}>
          {postlist}
        </Card.Group>
        {this.props.pages > 1 && (
          <Container textAlign='center' style={{margin: '20px'}}>
            <Pagination
              activePage={this.state.aPage}
              totalPages={pages}
              firstItem={false}
              lastItem={false}
              onPageChange={this.handlePaginationChange}
            />
          </Container>
        )}
      </Container>
    )
  }
}

function mapStateToProps({post, topic, comment}, ownProps){
  let pages = Math.floor(post.length / 9)
  let topicLocation = ownProps.match.params.topicLocation ? ownProps.match.params.topicLocation : 'recent'
  if(ownProps.location.pathname === '/profile')
    topicLocation = 'profile'
  if(post.length % 9 > 0)
    pages += 1

  return {
    total: post.length,
    pages: pages,
    posts: post,
    topicLocation: topicLocation,
    topics: topic,
    comments: comment
  }
}

function mapDispachToProps(dispatch){
  return{
    getPostsFrom: (data) => dispatch(getPostsFromAPI(data))
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(PostCardGrid));
