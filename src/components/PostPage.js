import React, {Component} from 'react'
import {connect} from 'react-redux'
import {withRouter, Link} from 'react-router-dom'
import {Divider, Container, Grid, Header, Icon, Button, Modal} from 'semantic-ui-react'

import {getSinglePostAPI, deletePostAPI, editPostAPI} from './../actions/post'
import {timeSince} from './../utils/helpers'

import PageNotFound from './PageNotFound'
import CommentThread from './CommentThread'
import Avatar from './Avatar'
import EditPostModal from './EditPostModal'
import AuthOnly from './AuthOnly'


class PostPage extends Component{

  constructor(props) {
    super(props);

    this.state = {
      post: props.post,
      showDeleteModal: false,
      editBody: props.post ? props.post.body : '',
      pageNotFound: props.pageNotFound,
    };
  }

  componentDidMount(){
    if(!this.state.post){
      this.props.getSinglePost({postid: this.props.postid, location: this.props.topicLocation})
      .then(data => {
        if(!data.error){
          this.setState({post: data.data})
        }
        else{
          this.setState({pageNotFound: true})
        }
      })
    }
  }

  onDeletePost = () => {
    this.props.deletePost({id: this.props.postid})
    this.setState({showDeleteModal: false})
    this.props.history.push('/dash')
  }

  onEditPost = (id, body) => {
    this.props.editPost({postid: id, body})
    .then(data => {
      if(!data.error){
        this.setState({post: data.data, })
      }
    })
  }

  render(){
    const post = this.state.post

    const deleteModal = (
      <Modal open={this.state.showDeleteModal} basic size='small'>
        <Header icon='trash outline' content='Delete Post' />
        <Modal.Content>
          <p>Are you sure you want to delete this post?  This action will remove this post and all of its comments.</p>
        </Modal.Content>
        <Modal.Actions>
          <Button color='red' inverted onClick={() => this.setState({showDeleteModal: false})}>
            <Icon name='remove' /> No
          </Button>
          <Button color='green' inverted onClick={() => this.onDeletePost()}>
            <Icon name='checkmark' /> Yes
          </Button>
        </Modal.Actions>
      </Modal>
    )

    return(
      <div>

        {this.state.pageNotFound && (
          <PageNotFound/>
        )}
        {post && (
          <Container>
            {deleteModal}
            <Grid>
              <Grid.Row>
                <Grid.Column width='4'>
                  <Header as='h2'>
                    <Avatar name={post.poster} />
                    <Header.Content style={{padding: '0px'}}>
                      {post.poster}
                      <Header.Subheader style={{fontSize: '0.95em'}}>
                        {timeSince(post.createdAt)}
                      </Header.Subheader>
                    </Header.Content>
                  </Header>
                </Grid.Column>
                <Grid.Column width='8' textAlign='center'>
                  <h1 style={{marginBottom: '0'}}>{post.title}</h1>
                  <h4 style={{marginBottom: '0', marginTop: '0'}}><Link to={`/topic/${this.props.topicLocation}`}>{this.props.topic.title}</Link></h4>
                </Grid.Column>
                <Grid.Column width='2' floated='right'>
                  {post.userId === this.props.user.id &&(
                    <Container>
                      <EditPostModal post={post} onEdit={this.onEditPost}/>
                      <Button
                        compact
                        basic
                        fluid
                        onClick={() => this.setState({showDeleteModal: true})}
                      >
                        <Icon name='delete'/>
                        Delete
                      </Button>
                    </Container>
                  )}
                </Grid.Column>
              </Grid.Row>
            </Grid>
            <Divider />
            <Container style={{whiteSpace: 'pre-line'}}>
              {post.body}
            </Container>
            <Divider />
            <CommentThread/>
          </Container>
        )}
        <AuthOnly/>
      </div>
    )
  }
}

function mapStateToProps({post, topic, user}, ownProps){
  let postid = ownProps.match.params.postid
  let location = ownProps.match.params.topicLocation
  let t = topic.find(item => item.location === location)
  let p = post.find(item => item.id === Number(postid) && item.topicId === t.id)

  return {
    postid: postid,
    topicLocation: location,
    post: p,
    user: user,
    topic: t,
  }
}

function mapDispachToProps(dispatch){
  return{
    getSinglePost: (data) => dispatch(getSinglePostAPI(data)),
    deletePost: (data) => dispatch(deletePostAPI(data)),
    editPost: (data) => dispatch(editPostAPI(data)),
  }
}

export default withRouter(connect(mapStateToProps, mapDispachToProps)(PostPage));
