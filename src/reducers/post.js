import {GET_POSTS, SEND_POST, GET_POSTS_FROM} from './../actions/post'
import {DELETE_POST} from './../actions/post'
import {LOGGED_OUT} from './../actions/post'

function post(state = [], action){

  if(action.type === GET_POSTS){
    const {data} = action

    return data
  }

  else if(action.type === GET_POSTS_FROM){
    const {data} = action
    let newState = state

    return newState.concat(data)
  }

  else if(action.type === SEND_POST){
    const {post} = action

    let newState = []
    newState.push(post)
    for(let i=0;i<state.length;i++){
      newState.push(state[i])
    }

    return newState
  }

  else if(action.type === DELETE_POST){
    let {id} = action

    let newState = []
    for(let i=0;i<state.length;i++){
      if(state[i].id !== Number(id)){
        newState.push(state[i])
      }
    }
    return newState
  }

  else if(action.type === LOGGED_OUT){
    return []
  }

  else
    return state

}

export default post;
