import {NEW_USER, LOGGED_IN, LOGGED_OUT} from '../actions/user'

const defaultState = {
  first: null,
  last: null,
  username: null,
  email: null,
  userLoggedIn: false
}

function user(state = defaultState, action){

  if(action.type === NEW_USER){
    const {first, last, username, email} = action
    let p = {first, last, username, email, userLoggedIn: false}
    return p
  }

  else if(action.type === LOGGED_IN){
    const {first, last, username, email, id, confirmed, token} = action
    let p = {first, last, username, email, userLoggedIn: true, id, confirmed, token}
    return p
  }

  else if(action.type === LOGGED_OUT){
    return {
      first: null,
      last: null,
      username: null,
      email: null,
      userLoggedIn: false
    }
  }

  else
    return state

}

export default user;
