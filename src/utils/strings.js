import React from 'react'

export const website = 'https://api.theblueboard.org'
export const localhost = 'http://localhost:3001'

export const whatIsBluBoard = (
  <div>
    BlueBoard is a forum built exclusively for Coronado teachers to encourage
    and support open discussion, sharing of ideas, collaboration, and recreation
    among coworkers.  Because Coronado is such a large campus, most teachers
    will never engage with 80% of their colleagues.  BlueBoard is an attempt to
    unify our campus by providing a common, accessible space for teachers to
    exchange ideas and get to know each other. 
  </div>
)

export const termsAndContions = (
  <div>
    <p>
      Your access to and use of the Service is conditioned upon your acceptance of
      and compliance with these Terms. These Terms apply to all visitors, users and
      others who wish to access or use the Service.
    </p>
    <p>
      By accessing or using the Service you agree to be bound by these Terms. If
      you disagree with any part of the terms then you do not have permission to
      access the Service.
    </p>
    <h3>Content</h3>
    <p>
      You the user agree to take sole responsibility for all actions taken while using this service.
      Furthermore, you agree to verify that all posted material, links, comments, and content is
      work appropriate and legally allowed to be posted and shared.
    </p>
    <p>
      Additionally, BlueBoard will not allow content where the sole purpose is to bully, harass,
      belittle, insult, or otherwise be hurtful to other individuals. Such content will be removed
      immediately.
    </p>
    <h3>Accounts</h3>
    <p>
      By creating an account, you certify that you are a full-time teacher at Coronado High School,
      that all information provided during the signup process is complete and correct, and that
      you are the owner of the email address provided.
    </p>
    <h3>Termination</h3>
    <p>
      BlueBoard may terminate or suspend your account and bar access to the Service
      immediately, without prior notice or liability, under our sole discretion,
      for any reason whatsoever and without limitation, including but not limited
      to a breach of the Terms.
    </p>
    <p>
      Furthermore, BlueBoard may remove or censor any content for any reason,
      including but not limited to a breach of the Terms.
    </p>
    <p>
      If you wish to terminate your account, you may simply discontinue using the Service.
    </p>
    <p>
      All provisions of the Terms which by their nature should survive
      termination shall survive termination, including, without limitation,
      ownership provisions, warranty disclaimers, indemnity and limitations of
      liability.
    </p>
    <h3>Disclaimer</h3>
    <p>
      You agree to defend, indemnify and hold harmless BlueBoard and its licensee
      and licensors, and their employees, contractors, agents, officers and
      directors, from and against any and all claims, damages, obligations,
      losses, liabilities, costs or debt, and expenses (including but not limited
      to attorney fees), resulting from or arising out of a) your use and
      access of the Service, by you or any person using your account and
      password; b) a breach of these Terms, or c) Content posted on the Service.
    </p>
    <p>
      In no event shall BlueBoard, nor its directors, employees, partners, agents,
      suppliers, or affiliates, be liable for any indirect, incidental, special,
      consequential or punitive damages, including without limitation, loss of
      profits, data, use, goodwill, or other intangible losses, resulting from
      (i) your access to or use of or inability to access or use the Service;
      (ii) any conduct or content of any third party on the Service; (iii) any
      content obtained from the Service; and (iv) unauthorized access, use or
      alteration of your transmissions or content, whether based on warranty,
      contract, tort (including negligence) or any other legal theory, whether
      or not we have been informed of the possibility of such damage, and even
      if a remedy set forth herein is found to have failed of its essential purpose.
    </p>
    <p>
      Your use of the Service is at your sole risk. The Service is provided on
      an "AS IS" and "AS AVAILABLE" basis. The Service is provided without
      warranties of any kind, whether express or implied, including, but not
      limited to, implied warranties of merchantability, fitness for a particular
      purpose, non- infringement or course of performance.
    </p>
    <p>
      BlueBoard its subsidiaries, affiliates, and its licensors do not warrant
      that a) the Service will function uninterrupted, secure or available at
      any particular time or location; b) any errors or defects will be corrected;
      c) the Service is free of viruses or other harmful components; or d) the
      results of using the Service will meet your requirements.
    </p>

  </div>
)
