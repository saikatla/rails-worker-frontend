function length(length, string){
  if(string && string.length >= length)
    return true;
  else if(string && string.length > 0)
    return false;
}

function hasNumber(string) {
  if(string && /\d/.test(string))
    return true
  else if(string)
    return false
}

export function password(pass){
  return length(pass) && hasNumber(pass)
}

export function email(email){
  if(email && email.endsWith('@episd.org'))
    return 'success'
  else if(email)
    return 'error'
}

export function confirmPassword(password, confirm){
  if(password && confirm && password === confirm)
    return 'success'
  else if(password && confirm)
    return 'error'
}
